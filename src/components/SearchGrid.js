import React, { Component } from "react";
import { Card, Icon, Row, Col, Input } from "antd";
import "./SearchGrid.css";
import { connect } from "react-redux";
import * as searchActions from "../actions/searchActions";
const { Meta } = Card;
const Search = Input.Search;
class SearchGrid extends Component {
  searchItems = e => {
    this.props.onSearch(e.target.value);
  };
  renderListCards() {
    const tmpProductsList = this.props.searchKeyWord
      ? this.props.products.filter(product =>
          product.name.match(this.props.searchKeyWord)
        )
      : this.props.products;

    const productsList =
      tmpProductsList.length !== 0 ? (
        tmpProductsList.map((product, index) => (
          <Col key={index} span={8}>
            <Card
              style={{ width: 400, margin: 10 }}
              cover={
                <span>
                  <img alt={product.name} src={product.picture} />
                </span>
              }
              actions={[
                <span>
                  <Icon type="shopping-cart" /> {"Add To Cart"}
                </span>
              ]}
            >
              <Meta title={product.name} description={product.price + "L.E"} />
            </Card>
          </Col>
        ))
      ) : (
        <div className="product-notFound">
          <p>{"NO PRODUCT WAS FOUND"}</p>
        </div>
      );
    return <Row gutter={16}>{productsList} </Row>;
  }
  render() {
    return (
      <div>
        <header className="search-page-header">
          <h1 className="header-title">{"Search"}</h1>
        </header>
        <div className="search-page-body">
          <div className="list-grid">
            <div>
              <Search
                placeholder="input search text"
                onChange={this.searchItems}
              />
            </div>
            {this.renderListCards()}
          </div>
        </div>
      </div>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    onSearch: value => dispatch(searchActions.onSearch(value))
  };
}
function mapStateToProps(state, props) {
  return {
    products: state.products.products,
    searchKeyWord: state.products.inputValue
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchGrid);
