import { combineReducers } from "redux";
import products from "./searchReducer";
const rootReducer = combineReducers({
  products: products
});

export default rootReducer;
