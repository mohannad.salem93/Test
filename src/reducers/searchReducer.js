import Products from "../Products.json";
export default function searchReducer(
  state = { inputValue: "", products: Products },
  action
) {
  switch (action.type) {
    case "Search_Products":
      return Object.assign({}, { ...state, inputValue: action.inputValue });

    default:
      return state;
  }
}
