import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import SearchGrid from "./components/SearchGrid";
import registerServiceWorker from "./registerServiceWorker";
import configureStore from "./store/configureStore";

const store = configureStore();
ReactDOM.render(
  <Provider store={store}>
    <SearchGrid />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
