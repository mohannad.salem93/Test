export function onSearch(value) {
  return { type: "Search_Products", inputValue: value };
}
